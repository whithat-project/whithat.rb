# frozen_string_literal: true

Gem::Specification.new do |spec|
	spec.name = "whithat"
	spec.version = "0.0.0"
	spec.authors = ["Rintim"]
	spec.email = ["rintim@foxmail.com"]

	spec.summary = "Another dumb downloader that scrapes the web, which is not only a cilent."
	spec.description = "Whithat is a dumb downloader like you-get ..."
	spec.homepage = "https://whithat-project.io"
	spec.required_ruby_version = ">= 3.1.0"

	# spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"
	spec.metadata["rubygems_mfa_required"] = "true"

	spec.metadata["homepage_uri"] = spec.homepage
	spec.metadata["source_code_uri"] = "https://gitlab.com/whithat-project/Whithat.rb"
	spec.metadata["changelog_uri"] = "https://whithat-project.io/changelog"

	# Specify which files should be added to the gem when it is released.
	# The `git ls-files -z` loads the files in the RubyGem that have been added into git.
	spec.files = Dir.chdir(File.expand_path(__dir__)) do
		`git ls-files -z`.split("\x0").reject do |f|
			(f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
		end
	end
	spec.bindir = "exe"
	spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
	spec.require_paths = %w[src]

	# Uncomment to register a new dependency of your gem
	# spec.add_dependency "example-gem", "~> 1.0"

	# For more information and examples about making a new gem, check out our
	# guide at: https://bundler.io/guides/creating_gem.html
end
