# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../src", __dir__)
require "whithat"

require "minitest/autorun"
